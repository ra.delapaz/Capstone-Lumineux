package myapp.capstone.com.lumineux.alarms;

/**
 * Created by ra.delapaz on 4/10/2018.
 */

public class RequestCode {

    int requestCode;
    long timeInMillis;

    public RequestCode(int requestCode, long timeInMillis){
        this.requestCode = requestCode;
        this.timeInMillis = timeInMillis;
    }

    public int getRequestCode(){
        return requestCode;
    }

    public long getTimeInMillis(){
        return timeInMillis;
    }
}
