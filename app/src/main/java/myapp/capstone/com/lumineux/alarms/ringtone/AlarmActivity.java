package myapp.capstone.com.lumineux.alarms.ringtone;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Calendar;

import myapp.capstone.com.lumineux.R;
import myapp.capstone.com.lumineux.alarms.misc.AlarmController;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by ra.delapaz on 4/10/2018.
 */

public class AlarmActivity extends RingtoneActivity {

    private NotificationManager mNotificationManager;
    private AlarmController alarmController;
    public int requestCode;
    private Calendar currentTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent controllerIntent = getIntent();
        requestCode = controllerIntent.getIntExtra("requestCode",0);
//
//        currentTime = Calendar.getInstance();
//        currentTime.getTime();
        Log.d("AlarmActivity","getTime");
        alarmController = new AlarmController(this);
//        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

//    @Override
//    protected int getAutoSilencedText() {
//        return R.string.alarmactivity_autosilence;
//    }

    @Override
    protected int getLeftButtonText() {
        return R.string.alarmactivity_snooze;
    }

    @Override
    protected int getRightButtonText() {
        return R.string.alarmactivity_dismiss;
    }

    @Override
    protected int getLeftButtonDrawable() {
        return R.drawable.alarm_snooze_48dp;
    }

    @Override
    protected int getRightButtonDrawable() {
        return R.drawable.alarm_dismiss_48dp;
    }

    @Override
    protected void onLeftButtonClick() {
        Toast.makeText(this, "You are in the alarm activity", Toast.LENGTH_SHORT).show();
        alarmController.cancelAlarm(requestCode);
        Toast.makeText(this, "Alarm Dismissed", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRightButtonClick() {
        alarmController.cancelAlarm(requestCode);
        Toast.makeText(this, "Alarm Dismissed: "+  requestCode, Toast.LENGTH_SHORT).show();
        finish();
    }




//    @Override
//    protected Class<? extends RingtoneService> getRingtoneServiceClass() {
//        return AlarmRingtoneService.class;
//    }

}
