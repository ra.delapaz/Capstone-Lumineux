package myapp.capstone.com.lumineux.alarms.misc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.AlphabeticIndex;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.capstone.com.lumineux.RecordingListAdapter;
import myapp.capstone.com.lumineux.alarms.Alarm;
import myapp.capstone.com.lumineux.alarms.RequestCode;
import myapp.capstone.com.lumineux.alarms.background.AlarmReceiver;
import myapp.capstone.com.lumineux.alarms.ringtone.AlarmActivity;
import myapp.capstone.com.lumineux.alarms.ringtone.RingtoneService;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ra.delapaz on 4/3/2018.
 */

public class AlarmController {
    private static final String TAG = "Alarm Controller";
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private Intent alarmIntent;
    public long alarmSet;
    public long snoozeTime;
    public Calendar calendar;



    private final Context mAppContext;
    public Map<String,String> myMap = new HashMap<String, String>();

    public AlarmController(Context context){
        mAppContext = context.getApplicationContext();

    }

    /**When the time set alarms, it will go to the receiver class, and the AlarmRec
     *  must go to AlarmActivity**/

    public void scheduleAlarm(long time,int requestCode){
        calendar = Calendar.getInstance();

         alarmSet = time;
        alarmManager = (AlarmManager) mAppContext.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(mAppContext,AlarmActivity.class)
                .putExtra("requestCode",requestCode);
        pendingIntent = PendingIntent.getActivity(mAppContext,requestCode,alarmIntent,PendingIntent.FLAG_UPDATE_CURRENT);
//      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,alarmSet,pendingIntent);
//      } else {
//            alarmManager.set(AlarmManager.RTC_WAKEUP,alarmSet,pendingIntent);
//      }
        Toast.makeText(mAppContext, "Current Time: "+ System.currentTimeMillis(), Toast.LENGTH_SHORT).show();

        Toast.makeText(mAppContext, "Alarm set to: " + alarmSet, Toast.LENGTH_LONG).show();


//        RequestCode getStoredCode = new RequestCode(requestCode,time);
//        requestCodeArrayList.add(getStoredCode);
//
        myMap.put(String.valueOf(requestCode),String.valueOf(time));

        SharedPreferences keyValues = mAppContext.getSharedPreferences("alarmData",Context.MODE_PRIVATE);
        SharedPreferences.Editor keyValuesEditor = keyValues.edit();

        for (String s : myMap.keySet()) {
            keyValuesEditor.putString(s, myMap.get(s));
        }

        keyValuesEditor.commit();



        Toast.makeText(mAppContext, "You are in the Controller " +alarmSet, Toast.LENGTH_SHORT).show();
//        Toast.makeText(mAppContext, "The request code is "+ requestCode, Toast.LENGTH_SHORT).show();

        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            Toast.makeText(mAppContext, "Map " +entry.getKey()+" : "+entry.getValue(), Toast.LENGTH_SHORT).show();
        }

    }

    public void snoozeAlarm(long snoozedTime){

        long minutesToSnooze = 300000;

    }

    public void cancelAlarm(int id){

        alarmManager = (AlarmManager) mAppContext.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(mAppContext, AlarmActivity.class);
        pendingIntent = PendingIntent.getActivity(mAppContext, id, alarmIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();



    }

    public static String getDate(long milliSeconds) {
        String dateFormat =  "dd/MM/yyyy hh:mm:ss.SSS";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

//    public long ringsAt() {
//        return alarmSet - System.currentTimeMillis();
//    }





}
