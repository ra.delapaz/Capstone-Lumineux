package myapp.capstone.com.lumineux.alarms.ringtone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import myapp.capstone.com.lumineux.BaseActivity;
import myapp.capstone.com.lumineux.R;

/**
 * Created by ra.delapaz on 4/10/2018.
 */

public abstract class RingtoneActivity extends BaseActivity {


    @BindView(R.id.title)
    TextView mHeaderTitle;
//    @BindView(R.id.auto_silenced_container)
//    LinearLayout mAutoSilencedContainer;
//    @BindView(R.id.auto_silenced_text) TextView showAutoSilenceText;
    @BindView(R.id.buttons_container) LinearLayout mButtonsContainer;
    @BindView(R.id.btn_text_left) TextView leftButtonView;
    @BindView(R.id.btn_text_right) TextView rightButtonView;


//
//
//    @DrawableRes
//    protected int getAutoSilencedDrawable() {
//        return 0;
//    }
//
//    @StringRes
//    protected abstract int getAutoSilencedText();

    @StringRes
    protected abstract int getLeftButtonText();

    @StringRes
    protected abstract int getRightButtonText();

    @DrawableRes
    protected abstract int getLeftButtonDrawable();

    @DrawableRes
    protected abstract int getRightButtonDrawable();

    @OnClick(R.id.btn_left)
    protected abstract void onLeftButtonClick();

    @OnClick(R.id.btn_right)
    protected abstract void onRightButtonClick();

//    protected abstract Class<? extends RingtoneService> getRingtoneServiceClass();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);


//        mHeaderTitle.setText(getHeaderTitle()); // TOneverDO: call before assigning mRingingObject
//        showAutoSilenceText.setCompoundDrawablesWithIntrinsicBounds(0, getAutoSilencedDrawable(), 0, 0);
//        showAutoSilenceText.setText(getAutoSilencedText());
        leftButtonView.setText(getLeftButtonText());
        rightButtonView.setText(getRightButtonText());
        leftButtonView.setCompoundDrawablesWithIntrinsicBounds(0, getLeftButtonDrawable(), 0, 0);
        rightButtonView.setCompoundDrawablesWithIntrinsicBounds(0, getRightButtonDrawable(), 0, 0);

//
//        Intent intent = new Intent(this, getRingtoneServiceClass());
////                .putExtra(); // String name, int Value;
//        startService(intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE;

            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
    }

    // TODO: Do we need this anymore? I think this broadcast was only sent from
    // EditAlarmActivity?
//    private final BroadcastReceiver mFinishReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            stopAndFinish();
//        }
//    };



    @Override
    protected int layoutResId() {
        return R.layout.alarm_layout;
    }

//    protected final void stopAndFinish() {
//        stopService(new Intent(this, getRingtoneServiceClass()));
//        finish();
//    }



}
