package myapp.capstone.com.lumineux.category;


public enum PeopleEnum {
    posing,
    smiling,
    people,
    people_young;

    public static boolean hasTag(String tag){
        for(PeopleEnum p : values()){
            if (p.name().equals(tag))
                return true;
        }
        return false;
    }
}
