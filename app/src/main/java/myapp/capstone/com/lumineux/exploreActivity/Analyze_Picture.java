package myapp.capstone.com.lumineux.exploreActivity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;
import com.microsoft.projectoxford.vision.contract.Description;
import com.microsoft.projectoxford.vision.contract.Tag;
import com.microsoft.projectoxford.vision.rest.VisionServiceException;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import myapp.capstone.com.lumineux.ExploreActivity;
import myapp.capstone.com.lumineux.MainActivity;
import myapp.capstone.com.lumineux.R;
import myapp.capstone.com.lumineux.camera.Camera_AnalyzeImage;
import myapp.capstone.com.lumineux.category.LandmarkEnum;
import myapp.capstone.com.lumineux.category.PeopleEnum;
import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

public class Analyze_Picture extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    public String output;
    // The edit to show status and result.
    private TextView mEditText;
    private static final String TAG = "Analyze_picture" ;
    private VisionServiceClient client;
    private Bitmap bitmapPicture;
    private Uri filename;
    ProgressDialog mProgressDialog;
    private RelativeLayout bitmaprelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.analyzepicturelayout);

        // Get a Uri from an Intent
        filename = getIntent().getParcelableExtra("imageUri");
        bitmaprelative = findViewById(R.id.analyze_layout);

        try {
            bitmapPicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filename);
        } catch (Exception e) {
            e.printStackTrace();
        }

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        readyTTS = true;
                        Log.e("TTS", "readyTTS True");
                    }
                } else {
                    Log.e("TTS", "Initialization Failed!");
                }
            }
        } );

        mEditText = findViewById(R.id.editTextResult);
        mEditText.setMovementMethod(new ScrollingMovementMethod());
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setTitle(getString(R.string.progress_dialog_title));

        // find the width and height of the screen:
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        // scale it to fit the screen, x and y swapped because my image is wider than it is tall
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

        BitmapDrawable data2 = new BitmapDrawable(getResources(), rotatedBitmap);
        Log.i(TAG, "rotated bitmap");

        bitmaprelative.setBackground(data2);

        if (client == null) {
            client = new VisionServiceRestClient(getString(R.string.subscription_key), getString(R.string.subscription_apiroot));
        }

        doAnalyze();
    }

    public void doAnalyze() {
        try {
            new doRequest().execute();
        } catch (final Exception e) {
            output = "Error encountered. Exception is: " + e.toString();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    speakOut(output);  //speak after 1000ms
                }
            }, 500);

            mEditText.setText(output);
        }
    }

    private String process() throws VisionServiceException, IOException {
        Gson gson = new Gson();
        String[] features = {"Color", "Faces", "Adult", "Categories", "Description","Tags"};
        String[] details = {};
        // Put the image into an input stream for detection.
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
        AnalysisResult v = this.client.analyzeImage(inputStream, features, details);

        String result = gson.toJson(v);
        Log.d("result", result);

        return result;
    }

    private class doRequest extends AsyncTask<String, String, String> {
        private Exception e = null;

        public doRequest() {
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
            Log.i("Request", "Analyzing image...");
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                return process();
            } catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            mProgressDialog.dismiss();
            mEditText.setText("");

            if (e != null) {
                final String errorMessage = e.getMessage();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        speakOut(errorMessage);  //speak after 1000ms
                    }
                }, 500);

                mEditText.setText(errorMessage);
                output = errorMessage;
                this.e = null;
            } else {
                Gson gson = new Gson();
                AnalysisResult result = gson.fromJson(data, AnalysisResult.class);
                boolean hasTag = false;
                boolean hasLandmark = false;
                    for (final Tag tags : result.tags) {
                        if(PeopleEnum.hasTag(tags.name)) {
                            hasTag = true;
                            break;
                        }
                        if(LandmarkEnum.hasLandmark(tags.name)) {
                            hasLandmark = true;
                            break;
                        }
                    }
                if(hasTag) {
                    Intent in1 = new Intent(Analyze_Picture.this,  Analyze_Celebrity.class);
                    in1.putExtra("imageUri", filename);
                    startActivity(in1);
                } else {
                    if(hasLandmark) {
                        Intent in2= new Intent(Analyze_Picture.this,  Analyze_Landmark.class);
                        in2.putExtra("imageUri", filename);
                        startActivity(in2);
                    }

                    for (final Caption caption: result.description.captions) {
                        output = "You are seeing " + caption.text;

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                speakOut(output);  //speak after 1000ms
                            }
                        }, 500);

                    }
                    mEditText.setText(output);
                    mEditText.append("\n");
                }
            }
        }
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.RESULTCAMERA_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDoubleTap() {
        super.onDoubleTap();
        speakOut(output);
    }

    @Override
    public void onLongPress() {
        super.onLongPress();
        startActivity(new Intent(this, Camera_AnalyzeImage.class));
    }

    @Override
    public void onSwipeRight() {
        super.onSwipeRight();
        startActivity(new Intent(this, ExploreActivity.class));
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void speakOut(String text) {
        if(!readyTTS) {
            return;
        } else {
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}


