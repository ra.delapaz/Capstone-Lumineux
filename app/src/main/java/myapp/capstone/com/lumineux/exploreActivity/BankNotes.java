package myapp.capstone.com.lumineux.exploreActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;


import myapp.capstone.com.lumineux.ExploreActivity;
import myapp.capstone.com.lumineux.MainActivity;
import myapp.capstone.com.lumineux.R;
import myapp.capstone.com.lumineux.camera.Camera_BankNotes;
import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class BankNotes extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    public String output;
    // The edit to show status and result.
    private TextView mEditText;
    private Bitmap bitmapPicture;
    private Uri filename;
    private static final String TAG = "Analyze_Banknotes" ;
    ProgressDialog mProgressDialog;
    private RelativeLayout bitmaprelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.analyzepicturelayout);
        // Get a Uri from an Intent
        filename = getIntent().getParcelableExtra("imageUri");
        bitmaprelative = findViewById(R.id.analyze_layout);

        try {
            bitmapPicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filename);
        } catch (Exception e) {
            e.printStackTrace();
        }

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        readyTTS = true;
                        Log.e("TTS", "readyTTS True");
                    }
                } else {
                    Log.e("TTS", "Initialization Failed!");
                }
            }
        } );

        mEditText = findViewById(R.id.editTextResult);
        mEditText.setMovementMethod(new ScrollingMovementMethod());
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setTitle(getString(R.string.progress_dialog_title));

        // find the width and height of the screen:
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        // scale it to fit the screen, x and y swapped because my image is wider than it is tall
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

        BitmapDrawable data2 = new BitmapDrawable(getResources(), rotatedBitmap);
        Log.i(TAG, "rotated bitmap");

        bitmaprelative.setBackground(data2);

        doAnalyze();

    }

    public void doAnalyze() {
        try {
            new doRequest().execute();
        } catch (Exception e) {
            output = "Error encountered. Exception is: " + e.toString();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    speakOut(output);  //speak after 1000ms
                }
            }, 500);

            mEditText.setText(output);
        }
    }

    private String process() throws Exception{

        // Put the image into an input stream for detection.
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

        OkHttpClient client = new OkHttpClient();

        Request.Builder requestBuilder = new Request.Builder();
        RequestBody requestBody = null;

        requestBuilder.addHeader("Prediction-Key","a0b3ab2c3a5d4d218dfed15fdd2f98a6");
        requestBuilder.addHeader("Content-Type","application/json");
        requestBody = RequestBody.create(MediaType.parse("application/octet-stream; charset=utf-8"), output.toByteArray());

        requestBuilder.method("POST", requestBody);

        requestBuilder.url("https://southcentralus.api.cognitive.microsoft.com/customvision/v1.1/Prediction/4d2b9a91-0e72-4b1a-969b-ad2062237bdd/image?iterationId=1b54c374-cfb5-4575-a7b4-56ca27610234");

        Request request = requestBuilder.build();
        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            String result = response.body().string();
            return result;
        } else {
            throw new IOException("Exception: response code " + response.code());
        }
    }

    private class doRequest extends AsyncTask<String, String, String> {
        private Exception e = null;

        public doRequest() {
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
            Log.i("Request", "Analyzing image...");
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                return process();
            } catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            mProgressDialog.dismiss();
            mEditText.setText("");

            if (e != null) {
                final String errorMessage = e.getMessage();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        speakOut(errorMessage);  //speak after 1000ms
                    }
                }, 500);

                mEditText.setText(errorMessage);
                output = errorMessage;
                this.e = null;
            } else {
                try {
                    JSONObject predictionsArray = null;
                    predictionsArray = new JSONObject(data);
                    JSONArray pArray = predictionsArray.getJSONArray("Predictions");
                    JSONObject jobj = pArray.getJSONObject(0);
                    String tag = jobj.getString("Tag");
                    double probability  = jobj.getDouble("Probability");
                    NumberFormat probability2 = NumberFormat.getPercentInstance();
                    output = "I am " + probability2.format(probability) + " sure, this is a " + tag + " Philippine Peso Bill.";

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            speakOut(output);  //speak after 1000ms
                        }
                    }, 500);

                    mEditText.setText(output);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                mEditText.append("\n");
            }
        }
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.RESULTCAMERA_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDoubleTap() {
        super.onDoubleTap();
        speakOut(output);
    }

    @Override
    public void onLongPress() {
        super.onLongPress();
        startActivity(new Intent(this, Camera_BankNotes.class));
    }

    @Override
    public void onSwipeRight() {
        super.onSwipeRight();
        startActivity(new Intent(this, ExploreActivity.class));
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void speakOut(String text) {
        if(!readyTTS) {
            //Toast.makeText(this,"Text to Speech not ready ", Toast.LENGTH_LONG).show();
            return;
        } else {
            //Toast.makeText(this,"Text To Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
