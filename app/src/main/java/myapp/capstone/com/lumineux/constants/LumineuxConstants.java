package myapp.capstone.com.lumineux.constants;

import android.speech.tts.TextToSpeech;

/**
 * Created by ra.delapaz on 1/27/2018.
 */

public class LumineuxConstants {

    public static final String DATE_FORMATFILE = "yyyyMMdd_HHmmss";
    public static int queueType = TextToSpeech.QUEUE_FLUSH;

    //Home screen
    public static final String SPEECH_WELCOME ="Welcome to Luminu. " +
            "The assistive mobile application for visually impaired. " +
            "Tap the upper right corner within the application for the tutorials.";
    public static final String SPEECH_TUTORIAL ="Select a Mode. " +
            "Swipe left for Explore Mode, Swipe right for the Reminder Mode or Swipe Up to go to Settings. " +
            "Swipe down within the application to return to the main menu ";

    //Explore Mode
    public static final String SPEECH_EXPLORE = "Explore Mode. It can detect People, Landmarks, Objects, Text Materials and Philippine Bills.";
    public static final String EXPLORE_TUTORIAL  = "Select a mode. Tap the blue button for analyze image, " +
            "tap the green button for recognize text or tap the orange button for identify banknotes. Long tap the buttons for an intro.";
    public static final String ANALYZEIMAGE_INTRO  = "Analyze image can detect People, Celebrities, Landmarks and objects";
    public static final String RECOGNIZETEXT_INTRO  = "Recognize Text can read any text materials";
    public static final String BILLS_INTRO  = "Identify Banknotes can detect Philippine Peso Bills";
    public static final String CAMERA_TUTORIAL = "Tap anywhere to capture an image. Swipe right to return to Explore Menu";
    public static final String RESULTCAMERA_TUTORIAL = "Long tap to capture image again and Double tap to repeat result. " +
            "Swipe right to return to Explore Menu";

    //Settings Mode
    public static final String SPEECH_SETTINGS = "Settings Mode. It can customize the voice of Luminu.";
    public static final String SETTINGS_TUTORIAL  = "Voice Attribute can change the voice to male or female. " +
            "Speech Rate it can change the speed of the voice.";

    //Reminder Mode
    public static final String SPEECH_REMINDER = "Reminder Mode. It can record an audio to remind you of your daily tasks.";
    public static final String REMINDER_TUTORIAL = "Tap the orange button to start recording and tap again to stop. Swipe right to view recorded audios";
    public static final String REMINDERLIST_TUTORIAL = "Tap the orange button to play audio and tap again to stop. Tap the blue 0clock icon to set the alarm for the audio.";
    public static final String REMINDER_MODE_LISTEMP = "No Recordings Found";
    public static final String REMINDER_SAVED = "Recording Saved, Please select a date and time for the alarm";
}
