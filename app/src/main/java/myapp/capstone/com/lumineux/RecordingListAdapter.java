package myapp.capstone.com.lumineux;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.commons.lang.time.DurationFormatUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import myapp.capstone.com.lumineux.alarms.Alarm;
import myapp.capstone.com.lumineux.alarms.misc.AlarmController;

/**
 * Created by ra.delapaz on 3/21/2018.
 */

public class RecordingListAdapter extends RecyclerView.Adapter<RecordingListAdapter.ViewHolder> implements TimePickerDialog.OnTimeSetListener {
    private ArrayList<Recording> recordingArrayList;
    private Context context;
    private MediaPlayer mPlayer;
    private boolean isPlaying = false;
    private int last_index = -1;
    public int requestCode,dayWeek,selectHour,selectMinute;
    private Calendar dateNow = Calendar.getInstance();
    private Calendar calendar =(Calendar) dateNow.clone();
    public Alarm alarm;
    public final View v = null;
    private final boolean[] recurringDays = new boolean[7];
    public TimePickerDialog timePickerDialog;
    public RecordingListAdapter recordingListAdapter;
    public AlarmController alarmController;

//    private OnEntryClickListener mOnEntryClickListener;

    public RecordingListAdapter(Context context, ArrayList<Recording> recordingArrayList){
        this.context = context;
        this.recordingArrayList = recordingArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recording_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecordingListAdapter.ViewHolder holder, int position) {
        setUpData(holder,position);
    }

    public void setRecordingArrayList(ArrayList<Recording> recordingArrayList) {
        this.recordingArrayList = recordingArrayList;
    }

    @Override
    public int getItemCount() {
        return recordingArrayList.size();
    }

    private void setUpData(ViewHolder holder, int position) {
        Recording recording = recordingArrayList.get(position);
        holder.textViewName.setText(recording.getFileName());

        if(recording.isPlaying()) {
            holder.imageViewPlay.setImageResource(R.drawable.audiopause);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);
            }

            holder.seekBar.setVisibility(View.VISIBLE);
            holder.seekUpdation(holder);
        } else {
            holder.imageViewPlay.setImageResource(R.drawable.audioplay);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);
            }

            holder.seekBar.setVisibility(View.GONE);
        }

        holder.manageSeekBar(holder);
    }

    private void showSnackbar(final String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        alarm = new Alarm();
        selectHour =  hourOfDay;
        selectMinute = minute;

        calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK,dayWeek);
        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
        calendar.set(Calendar.MINUTE,minute);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);



        Toast.makeText(context, "Rings in "+ alarm.toString(context,ringsIn(),false), Toast.LENGTH_SHORT).show();

        AlarmController alarmController = new AlarmController(context);
        alarmController.scheduleAlarm(checkAlarm(),requestCode);


        SharedPreferences pref = context.getSharedPreferences("alarmData", Context.MODE_PRIVATE);
        HashMap<String, String> map= (HashMap<String, String>) pref.getAll();
        for (String s : map.keySet()) {

            String value=map.get(s);
            Toast.makeText(context, "Value "+Long.parseLong(value), Toast.LENGTH_SHORT).show();
        }


    }

    public boolean isRecurring(int day) {
        checkDay(day);
        return recurringDays[day];
    }

    public boolean hasRecurrence() {
        return numRecurringDays() > 0;
    }

    public int numRecurringDays() {
        int count = 0;

        for (boolean b : recurringDays)
            if (b) count++;
        return count;
    }

    public long checkAlarm(){

        calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK,dayWeek);
        calendar.set(Calendar.HOUR_OF_DAY, selectHour);
        calendar.set(Calendar.MINUTE, selectMinute);
        calendar.set(Calendar.SECOND, 0);
        long baseRingTime = calendar.getTimeInMillis();

        if (!hasRecurrence()) {
            if (baseRingTime <= System.currentTimeMillis()) {
                // The specified time has passed for today
                baseRingTime += TimeUnit.DAYS.toMillis(7);
            }

            return baseRingTime;
        } else {
            // Compute the ring time just for the next closest recurring day.
            // Remember that day constants defined in the Calendar class are
            // not zero-based like ours, so we have to compensate with an offset
            // of magnitude one, with the appropriate sign based on the situation.
            int weekdayToday = calendar.get(Calendar.DAY_OF_WEEK);
            long numDaysFromToday = -1;

            for (int i = weekdayToday; i <= Calendar.SATURDAY; i++) {
                if (isRecurring(i -1  /*match up with our day constant*/)) {
                    if (i == weekdayToday) {
                        if (baseRingTime > System.currentTimeMillis()) {
                            // The normal ring time has not passed yet
                            numDaysFromToday = 0;
                            break;
                        }
                    } else {
                        numDaysFromToday = i + weekdayToday;
                        break;
                    }
                }
            }

            // Not computed yet
            if (numDaysFromToday < 0) {
                for (int i = Calendar.SUNDAY; i < weekdayToday; i++) {
                    if (isRecurring(i -1 /*match up with our day constant*/)) {
                        numDaysFromToday = Calendar.SATURDAY - weekdayToday + i;
                        break;
                    }
                }
            }
            if (numDaysFromToday < 0 && isRecurring(weekdayToday + 1)
                    && baseRingTime <= System.currentTimeMillis()) {
                numDaysFromToday = 7;
            }

            if (numDaysFromToday < 0)
                throw new IllegalStateException("How did we get here?");

            return baseRingTime + TimeUnit.DAYS.toMillis(numDaysFromToday);
        }
    }



    public long ringsIn() {
        return checkAlarm() - System.currentTimeMillis();
    }

    public static String getDate(long milliSeconds) {
        String dateFormat =  "dd/MM/yyyy hh:mm:ss.SSS";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private void checkDay(int day) {
        alarm = new Alarm();
        if (day < alarm.SUNDAY || day > alarm.SATURDAY) {
            throw new IllegalArgumentException("Invalid day of week: " + day);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageViewPlay;
        SeekBar seekBar;
        TextView textViewName;
        private String recordingUri;
        private int lastProgress = 0;
        private Handler mHandler = new Handler();
        ViewHolder holder;
        private LinearLayout linearLayout;
        private Button setAlarm;
        private AlertDialog alertDialog;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            imageViewPlay = itemView.findViewById(R.id.imageViewPlay);
            seekBar = itemView.findViewById(R.id.seekBar);
            textViewName = itemView.findViewById(R.id.textViewRecordingname);
            setAlarm = itemView.findViewById(R.id.setAlarm);

            textViewName.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Toast.makeText(context, String.valueOf("LONG PRESS"), Toast.LENGTH_SHORT).show();
                    int position = getAdapterPosition();
                    Recording recording = recordingArrayList.get(position);
                    recordingUri = recording.getUri();


                    //Delete
                    File file = new File(recordingUri);
                    file.delete();
                    recordingArrayList.remove(position);

                    return true;
                }
            });

            setAlarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    stopPlaying();
                    requestCode = getAdapterPosition();
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getRootView().getContext());
                    builder.setTitle("Select a day");
                    builder.setSingleChoiceItems(R.array.week, -1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            switch(item) {
                                case 0:

                                    dayWeek=  Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 1:
                                    //Monday
                                    Calendar.getInstance();
                                    dayWeek = 1;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 2:
                                    //Tuesday
                                    Calendar.getInstance();
                                    dayWeek = 2;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 3:
                                    //Wednesday
                                    Calendar.getInstance();
                                    dayWeek = 3;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 4:
                                    //Thursday
                                    Calendar.getInstance();
                                    dayWeek = 4;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 5:
                                    //Friday
                                    Calendar.getInstance();
                                    dayWeek=5;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 6:
                                    //Saturday
                                    Calendar.getInstance();
                                    dayWeek =6;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;
                                case 7:
                                    //Saturday
                                    dayWeek =7;
                                    timePickerDialog = new TimePickerDialog(v.getRootView().getContext(),RecordingListAdapter.this,selectHour,selectMinute,false);
                                    timePickerDialog.show();
                                    break;

                            }
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog = builder.create();
                    alertDialog.show();
                }
            });
        }

        private void markAllPaused() {
            for(int i =0; i < recordingArrayList.size(); i++){
                recordingArrayList.get(i).setPlaying(false);
                recordingArrayList.set(i,recordingArrayList.get(i));
            }
        }

        private void stopPlaying() {
            try {
                mPlayer.release();
            } catch(Exception e) {
                e.printStackTrace();
            }

            mPlayer = null;
            isPlaying= false;
        }

        private void startPlaying(final Recording audio, final int position) {
            mPlayer = new MediaPlayer();

            try {
                mPlayer.setDataSource(recordingUri);
                mPlayer.prepare();
                mPlayer.start();
            }catch(IOException e) {
                Log.e("LOG-E","IOException: prepare() failed");
            }

            seekBar.setMax(mPlayer.getDuration());
            isPlaying = true;

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audio.setPlaying(false);
                    notifyItemChanged(position);
                }
            });
        }

        public void manageSeekBar(ViewHolder holder) {
            holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (mPlayer != null && fromUser) {
                        mPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                seekUpdation(holder);
            }
        };

        private void seekUpdation(ViewHolder holder) {
            this.holder = holder;

            if(mPlayer != null) {
                int mCurrentPosition = mPlayer.getCurrentPosition() ;
                holder.seekBar.setMax(mPlayer.getDuration());
                holder.seekBar.setProgress(mCurrentPosition);
                lastProgress = mCurrentPosition;
            }

            mHandler.postDelayed(runnable, 100);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            int position = getAdapterPosition();
            Recording recording = recordingArrayList.get(position);
            recordingUri = recording.getUri();

            if(isPlaying) {
                stopPlaying();

                if(position == last_index){
                    recording.setPlaying(false);
                    stopPlaying();
                    notifyItemChanged(position);
                } else {
                    markAllPaused();
                    recording.setPlaying(true);
                    notifyItemChanged(position);
                    startPlaying(recording,position);
                    last_index = position;
                }
            } else {
                startPlaying(recording,position);
                recording.setPlaying(true);
                seekBar.setMax(mPlayer.getDuration());
                Log.d("isPlaying","False");
                notifyItemChanged(position);
                last_index = position;
            }
        }
    }
}
