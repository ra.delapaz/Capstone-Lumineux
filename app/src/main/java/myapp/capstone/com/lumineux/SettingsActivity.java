package myapp.capstone.com.lumineux;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Locale;

import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

/**
 * Created by ra.delapaz on 1/19/2018.
 */

public class SettingsActivity extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    RadioGroup radioRate, radioVoice;
    RadioButton radio_male,radio_female, rate_slow, rate_normal, rate_fast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.settings_mode);

        radioVoice = findViewById(R.id.radiogroup_voice);
        radioRate = findViewById(R.id.radiogroup_rate);
        radio_male = findViewById(R.id.radio_male);
        radio_female =  findViewById(R.id.radio_female);
        rate_slow = findViewById(R.id.rate_slow);
        rate_normal = findViewById(R.id.rate_normal);
        rate_fast = findViewById(R.id.rate_fast);

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    int result = toSpeech.setLanguage(Locale.getDefault());

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                    } else {
                        readyTTS = true;
                    }
                } else {
                    Log.e("error", "Initilization Failed!");
                }
            }
        } );

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                speakOut(LumineuxConstants.SPEECH_SETTINGS);  //speak after 1000ms
            }
        }, 500);

        checkButtonRate();
        checkButtonVoice();

        radioVoice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                setVoice();
            }
        });

        radioRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                setRate();
            }
        });
    }

    public void checkButtonRate() {
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);

        if (speechRate == 0.5f) {
            rate_slow.setChecked(true);
            rate_normal.setChecked(false);
            rate_fast.setChecked(false);
        } else if (speechRate == 1.0f) {
            rate_slow.setChecked(false);
            rate_normal.setChecked(true);
            rate_fast.setChecked(false);
        } else if (speechRate == 1.5f) {
            rate_slow.setChecked(false);
            rate_normal.setChecked(false);
            rate_fast.setChecked(true);
        }
    }

    public void checkButtonVoice() {
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        String speechVoice = sharedPreferences.getString("speechVoice", "femaleVoice");

        if (speechVoice.equals("maleVoice")) {
            radio_male.setChecked(true);
            radio_female.setChecked(false);
        } else if (speechVoice.equals("femaleVoice")) {
            radio_male.setChecked(false);
            radio_female.setChecked(true);
        } else {
            radio_male.setChecked(false);
            radio_female.setChecked(false);
        }
    }

    public float getRate() {
        int checkedRadioButton = this.radioRate.getCheckedRadioButtonId();

        if (checkedRadioButton == R.id.rate_slow) {
            return 0.5f;
        } else if (checkedRadioButton == R.id.rate_normal) {
            return 1.0f;
        } else if(checkedRadioButton == R.id.rate_fast) {
            return 1.5f;
        }
        return 0;
    }

    public void setRate() {
        if (getRate() == 0.5f) {
            speakOut("This is a slow speech rate");
        } else if (getRate() == 1.0f) {
            speakOut("This is a normal speech rate");
        } else {
            speakOut("This is a fast speech rate");
        }
    }

    public String getVoice() {
        int checkedRadioVoice = this.radioVoice.getCheckedRadioButtonId();

        if (checkedRadioVoice == R.id.radio_male) {
            return "maleVoice";
        } else if (checkedRadioVoice == R.id.radio_female){
            return "femaleVoice";
        }
        return "";
    }

    public void setVoice() {
        if(getVoice().equals("maleVoice")) {
            speakOut("This is an example of a speech synthesis with a male voice");
        } else if(getVoice().equals("femaleVoice")) {
            speakOut("This is an example of a speech synthesis with a female voice");
        }
    }

    public void save(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat("speechRate", getRate());
        editor.putString("speechVoice", getVoice().toString());

        editor.commit();

        Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.SETTINGS_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
    }

    private void speakOut(String text) {
        toSpeech.setSpeechRate(getRate());

        if(getVoice().equals("maleVoice")) {
            toSpeech.setLanguage(Locale.UK);
        } else {
            toSpeech.setLanguage(Locale.US);
        }

        if(!readyTTS) {
           // Toast.makeText(this, "Text to Speech not ready", Toast.LENGTH_LONG).show();
            return;
        } else {
           // Toast.makeText(this, "Text to Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    } // speak the selected value

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
