package myapp.capstone.com.lumineux.alarms.ringtone;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import myapp.capstone.com.lumineux.Recording;
import myapp.capstone.com.lumineux.RecordingItemActivity;

import static android.text.format.DateFormat.getTimeFormat;

/**
 * Created by ra.delapaz on 4/10/2018.
 */

public class RingtoneService extends Service {


    MediaPlayer mediaPlayer;
    private ArrayList<Recording> recordingArrayList;
    private Uri recordingUri;
    private String recordingURL;
    public RecordingItemActivity recordingItemActivity;
    int start_id;
    // Get the current time
    public Calendar calendar;
    //Compare it to the current time
    public Long savedTime, getCurrentTime;
    public int savedURI;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String state = intent.getExtras().getString("state");

        assert state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                break;
            default:
                startId = 0;
                break;
        }





        Log.v("onStartCommand","Ringtong Service");
        recordingItemActivity.getRingtone();
        Recording recordedFile = recordingArrayList.get(savedURI);
        recordingURL = recordedFile.getUri();
        recordingUri = Uri.parse(recordingURL);
        mediaPlayer = MediaPlayer.create(this,recordingUri);

        return START_NOT_STICKY;
    }

    public static String formatTime(Context context, long millis) {
        return getTimeFormat(context).format(new Date(millis));
    }

    public void onAutoSilenced(){

    }

    @Override
    public void onCreate() {
        super.onCreate();




    }

    public int getRingtone(){

        calendar = Calendar.getInstance();
        getCurrentTime =  System.currentTimeMillis();

        SharedPreferences pref = this.getSharedPreferences("alarmData", Context.MODE_PRIVATE);
        HashMap<String, String> map= (HashMap<String, String>) pref.getAll();
        for (String key : map.keySet()) {

            if(getCurrentTime == Long.parseLong(map.get(key))){
                savedURI = Integer.parseInt(key);
            }

            String value=map.get(key);
            Toast.makeText(this, "Value "+Integer.parseInt(value) , Toast.LENGTH_SHORT).show();
        }

        return savedURI;

    }

    //
//    public Notification getForegroundNotification() {
//
//        String title = getString(R.string.alarm);
//        return new NotificationCompat.Builder(this)
//                // Required contents
//                .setSmallIcon(R.drawable.alarm_icon)
//                .setContentTitle(title)
//                .setContentText(formatTime(this, System.currentTimeMillis()))
//                .addAction(R.drawable.alarm_snooze,
//                        getString(R.string.snooze),
//                        getPendingIntent(ACTION_SNOOZE, getRingingObject().getIntId()))
//                .addAction(R.drawable.alarm_dismiss,
//                        getString(R.string.dismiss),
//                        getPendingIntent
//                .build();
//    }
//
//
//    public final void stopForeground(boolean removeNotification) {
//        stopForeground(removeNotification ? STOP_FOREGROUND_REMOVE : 0);
//    }
}
