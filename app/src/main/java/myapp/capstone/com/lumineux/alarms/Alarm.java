package myapp.capstone.com.lumineux.alarms;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import myapp.capstone.com.lumineux.R;

/**
 * Created by ra.delapaz on 4/2/2018.
 */

public class Alarm  {
    public static final int SUNDAY    = 1;
    public static final int MONDAY    = 2;
    public static final int TUESDAY   = 3;
    public static final int WEDNESDAY = 4;
    public static final int THURSDAY  = 5;
    public static final int FRIDAY    = 6;
    public static final int SATURDAY  = 7;
    public static final int NUM_DAYS  = 7;
    private final boolean[] recurringDays = new boolean[NUM_DAYS];
    public static final int DAYS = 0;
    public static final int HOURS = 1;
    public static final int MINUTES = 2;
    public static final int SECONDS = 3;
    public static final int MILLIS = 4;


    private int dayOftheWeek() {
        return 0;
    }

    public int hour() {
        return 0;
    }

    public int minutes() {
        return 0;
    }

    public Calendar calendar;

    public boolean[] recurringDays() {
        return recurringDays;
    }

    public void setRecurring(int day, boolean recurring) {
        checkDay(day);
        recurringDays[day] = recurring;
    }

    public boolean isRecurring(int day) {
        checkDay(day);
        return recurringDays[day];
    }

    public boolean hasRecurrence() {
        return numRecurringDays() > 0;
    }

    public int numRecurringDays() {
        int count = 0;
        for (boolean b : recurringDays)
            if (b) count++;
        return count;
    }



    public long checkAlarm(){

        calendar = new GregorianCalendar();
        calendar.set(Calendar.DAY_OF_WEEK,dayOftheWeek());
        calendar.set(Calendar.HOUR_OF_DAY, hour());
        calendar.set(Calendar.MINUTE, minutes());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);




        long baseRingTime = calendar.getTimeInMillis();

        if (!hasRecurrence()) {
            if (baseRingTime <= System.currentTimeMillis()) {
                // The specified time has passed for today
                baseRingTime += TimeUnit.DAYS.toMillis(1);
            }
            return baseRingTime;
        } else {
            // Compute the ring time just for the next closest recurring day.
            // Remember that day constants defined in the Calendar class are
            // not zero-based like ours, so we have to compensate with an offset
            // of magnitude one, with the appropriate sign based on the situation.
            int weekdayToday = calendar.get(Calendar.DAY_OF_WEEK);
            long numDaysFromToday = -1;

            for (int i = weekdayToday; i <= Calendar.SATURDAY; i++) {
                if (isRecurring(i - 1 /*match up with our day constant*/)) {
                    if (i == weekdayToday) {
                        if (baseRingTime > System.currentTimeMillis()) {
                            // The normal ring time has not passed yet
                            numDaysFromToday = 0;
                            break;
                        }
                    } else {
                        numDaysFromToday = i - weekdayToday;
                        break;
                    }
                }
            }

            // Not computed yet
            if (numDaysFromToday < 0) {
                for (int i = Calendar.SUNDAY; i < weekdayToday; i++) {
                    if (isRecurring(i - 1 /*match up with our day constant*/)) {
                        numDaysFromToday = Calendar.SATURDAY - weekdayToday + i;
                        break;
                    }
                }
            }

            // Still not computed yet. The only recurring day is weekdayToday,
            // and its normal ring time has already passed.
            if (numDaysFromToday < 0 && isRecurring(weekdayToday - 1)
                    && baseRingTime <= System.currentTimeMillis()) {
                numDaysFromToday = 7;
            }

            if (numDaysFromToday < 0)
                throw new IllegalStateException("How did we get here?");

            return baseRingTime + TimeUnit.DAYS.toMillis(numDaysFromToday);
        }
    }

    public long ringsIn() {
        return checkAlarm() - System.currentTimeMillis();
    }


    private static void checkDay(int day) {
        if (day < SUNDAY || day > SATURDAY) {
            throw new IllegalArgumentException("Invalid day of week: " + day);
        }
    }

    public static String toString(Context context, long millis, boolean abbreviate) {
        long[] fields = breakdown(millis);
        long numDays = fields[DAYS];
        long numHours = fields[HOURS];
        long numMins = fields[MINUTES];
        long numSecs = fields[SECONDS]; // only considered for rounding of minutes
        if (numSecs >= 31) {
            numMins++;
            numSecs = 0; // Not totally necessary since it won't be considered any more
            if (numMins == 60) {
                numHours++;
                numMins = 0;
                if (numHours == 24) {
                    numDays++;
                    numHours = 0;
                }
            }
        }

        int res;
        if (abbreviate) {
            res = getAbbreviatedStringRes(numDays, numHours, numMins);
        } else {
            res = getStringRes(numDays, numHours, numMins);
        }

        return context.getString(res, numDays, numHours, numMins);
    }

    public static long[] breakdown(long t) {
        return breakdown(t, TimeUnit.MILLISECONDS, false);
    }

    public static long[] breakdown(long t, @NonNull TimeUnit unit, boolean roundMillis) {
        long days = unit.toDays(t);
        long hours = unit.toHours(t) % 24;
        long minutes = unit.toMinutes(t) % 60;
        long seconds = unit.toSeconds(t) % 60;
        long msecs = unit.toMillis(t) % 1000;
        if (roundMillis) {
            if (msecs >= 500) {
                seconds++;
                msecs = 0;
                if (seconds == 60) {
                    minutes++;
                    seconds = 0;
                    if (minutes == 60) {
                        hours++;
                        minutes = 0;
                        if (hours == 24) {
                            days++;
                            hours = 0;
                        }
                    }
                }
            }
        }
        return new long[] { days, hours, minutes, seconds, msecs };
    }

    @StringRes
    private static int getStringRes(long numDays, long numHours, long numMins) {
        int res;
        if (numDays == 0) {
            if (numHours == 0) {
                if (numMins == 0) {
                    res = R.string.less_than_one_minute;
                } else if (numMins == 1) {
                    res = R.string.minute;
                } else {
                    res = R.string.minutes;
                }
            } else if (numHours == 1) {
                if (numMins == 0) {
                    res = R.string.hour;
                } else if (numMins == 1) {
                    res = R.string.hour_and_minute;
                } else {
                    res = R.string.hour_and_minutes;
                }
            } else {
                if (numMins == 0) {
                    res = R.string.hours;
                } else if (numMins == 1) {
                    res = R.string.hours_and_minute;
                } else {
                    res = R.string.hours_and_minutes;
                }
            }
        } else if (numDays == 1) {
            if (numHours == 0) {
                if (numMins == 0) {
                    res = R.string.day;
                } else if (numMins == 1) {
                    res = R.string.day_and_minute;
                } else {
                    res = R.string.day_and_minutes;
                }
            } else if (numHours == 1) {
                if (numMins == 0) {
                    res = R.string.day_and_hour;
                } else if (numMins == 1) {
                    res = R.string.day_hour_and_minute;
                } else {
                    res = R.string.day_hour_and_minutes;
                }
            } else {
                if (numMins == 0) {
                    res = R.string.day_and_hours;
                } else if (numMins == 1) {
                    res = R.string.day_hours_and_minute;
                } else {
                    res = R.string.day_hours_and_minutes;
                }
            }
        } else {
            if (numHours == 0) {
                if (numMins == 0) {
                    res = R.string.days;
                } else if (numMins == 1) {
                    res = R.string.days_and_minute;
                } else {
                    res = R.string.days_and_minutes;
                }
            } else if (numHours == 1) {
                if (numMins == 0) {
                    res = R.string.days_and_hour;
                } else if (numMins == 1) {
                    res = R.string.days_hour_and_minute;
                } else {
                    res = R.string.days_hour_and_minutes;
                }
            } else {
                if (numMins == 0) {
                    res = R.string.days_and_hours;
                } else if (numMins == 1) {
                    res = R.string.days_hours_and_minute;
                } else {
                    res = R.string.days_hours_and_minutes;
                }
            }
        }
        return res;
    }

    @StringRes
    private static int getAbbreviatedStringRes(long numDays, long numHours, long numMins) {
        int res;
        if (numDays == 0) {
            if (numHours == 0) {
                if (numMins == 0) {
                    res = R.string.abbrev_less_than_one_minute;
                } else {
                    res = R.string.abbrev_minutes;
                }
            } else {
                if (numMins == 0) {
                    res = R.string.abbrev_hours;
                } else {
                    res = R.string.abbrev_hours_and_minutes;
                }
            }
        } else {
            if (numHours == 0) {
                if (numMins == 0) {
                    res = R.string.abbrev_days;
                } else {
                    res = R.string.abbrev_days_and_minutes;
                }
            } else {
                if (numMins == 0) {
                    res = R.string.abbrev_days_and_hours;
                } else {
                    res = R.string.abbrev_days_hours_and_minutes;
                }
            }
        }
        return res;
    }



}
