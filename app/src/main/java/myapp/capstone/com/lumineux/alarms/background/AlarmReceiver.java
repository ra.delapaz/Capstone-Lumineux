package myapp.capstone.com.lumineux.alarms.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import myapp.capstone.com.lumineux.alarms.ringtone.AlarmActivity;
import myapp.capstone.com.lumineux.alarms.ringtone.AlarmRingtoneService;

/**
 * Created by ra.delapaz on 3/15/2018.
 */

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        String getValueIntent = intent.getExtras().getString("extra");
        Log.v("Receiver","You are on the receiver");
        Intent serviceIntent = new Intent(context,AlarmActivity.class);
        serviceIntent.putExtra("state",getValueIntent);
        context.startService(serviceIntent);

    }
}
