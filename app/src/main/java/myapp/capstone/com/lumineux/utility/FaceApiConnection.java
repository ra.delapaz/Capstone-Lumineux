package myapp.capstone.com.lumineux.utility;

/**
 * Created by user on 3/5/2018.
 */

import android.app.Application;

import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;

public class FaceApiConnection extends Application {

    public static FaceServiceClient getFaceServiceClient() {

        return new FaceServiceRestClient("https://southeastasia.api.cognitive.microsoft.com/face/v1.0", "28e1f3f4e2ce4c0fa011f06488ac06eb");
    }

}
