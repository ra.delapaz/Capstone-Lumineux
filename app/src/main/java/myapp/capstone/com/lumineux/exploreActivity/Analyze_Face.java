package myapp.capstone.com.lumineux.exploreActivity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.contract.Accessory;
import com.microsoft.projectoxford.face.contract.Emotion;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FacialHair;
import com.microsoft.projectoxford.face.contract.Hair;
import com.microsoft.projectoxford.face.contract.HeadPose;
import com.microsoft.projectoxford.face.contract.Makeup;
import com.microsoft.projectoxford.vision.contract.GenderEnum;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import myapp.capstone.com.lumineux.ExploreActivity;
import myapp.capstone.com.lumineux.Helper.ImageHelper;
import myapp.capstone.com.lumineux.MainActivity;
import myapp.capstone.com.lumineux.R;
import myapp.capstone.com.lumineux.camera.Camera_AnalyzeImage;
import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.FaceApiConnection;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

public class Analyze_Face extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    public String output;
    // The edit to show status and result.
    public TextView mEditText;
    //bitmap picture
    private Bitmap bitmapPicture,rotatedBitmap;

    // Progress dialog popped up when communicating with server.
    ProgressDialog mProgressDialog;
    // The URI of the image selected to detect.
    private Uri filename;
    private static final String TAG = "Analyze_face";
    private RelativeLayout bitmaprelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.analyzepicturelayout);
        // bitmaprelative = findViewById(R.id.analyze_layout);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_detection);

        // Get a Uri from an Intent
        filename = getIntent().getParcelableExtra("imageUri");

        try {
            /*FileInputStream is = this.openFileInput(filename);
            bitmapPicture = BitmapFactory.decodeStream(is);
            bitmapPicture = loadSizeLimitedBitmap(is, getContentResolver(), bitmapPicture);*/
            bitmapPicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filename);
            // is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        readyTTS = true;
                        Log.e("TTS", "readyTTS True");
                    }
                } else {
                    Log.e("TTS", "Initialization Failed!");
                }
            }
        } );

       /* ImageView viewBtimap = findViewById(R.id.bitmapview);
        mEditText = findViewById(R.id.editTextResult);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.progress_dialog_title));

        viewBtimap.setImageBitmap(bitmapPicture);
        viewBtimap.setRotation(90);
    */
      /*  mEditText = findViewById(R.id.editTextResult);
        mEditText.setMovementMethod(new ScrollingMovementMethod());*/

        // Show the detected faces on original image.

        BitmapDrawable data2 = new BitmapDrawable(getResources(), rotatedBitmap);
     //   bitmaprelative.setBackground(data2);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setTitle(getString(R.string.progress_dialog_title));
        // find the width and height of the screen:
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();
        //viewBitmap.setRotation(90);

        // scale it to fit the screen, x and y swapped because my image is wider than it is tall
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        detect();
        Log.i(TAG, "rotated bitmap");

        //  bitmaprelative.setBackground(data2);

    }
    // Save the activity state when it's going to stop.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("ImageUri", filename);
    }

    public void detect() {
        // Put the image into an input stream for detection.
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

//        BitmapDrawable data2 = new BitmapDrawable(getResources(), rotatedBitmap);
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

        Log.i("Look here!", "Image size:" + rotatedBitmap.getWidth()
                + "x" + rotatedBitmap.getHeight());
        // Start a background task to detect faces in the image.
        new DetectionTask().execute(inputStream);
    }

    // Background task of face detection.
    private class DetectionTask extends AsyncTask<InputStream, String, Face[]> {
        private boolean mSucceed = true;

        @Override
        protected Face[] doInBackground(InputStream... params) {
            // Get an instance of face service client to detect faces in image.
            FaceServiceClient faceServiceClient = FaceApiConnection.getFaceServiceClient();

            try {
                publishProgress("Detecting...");

                // Start detection.
                return faceServiceClient.detect(
                        params[0],  /* Input stream of image to detect */
                        true,       /* Whether to return face ID */
                        true,       /* Whether to return face landmarks */

                        new FaceServiceClient.FaceAttributeType[]{
                                FaceServiceClient.FaceAttributeType.Age,
                                FaceServiceClient.FaceAttributeType.Gender,
                                FaceServiceClient.FaceAttributeType.Smile,
                                FaceServiceClient.FaceAttributeType.Emotion,
                        });

            } catch (Exception e) {
                mSucceed = false;
                publishProgress(e.getMessage());
                Log.e("error", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
            Log.i("Request", "Detecting in image bitmapPicture");
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            mProgressDialog.setMessage(progress[0]);
            //setInfo(progress[0]);
        }

        @Override
        protected void onPostExecute(Face[] result) {
            if (mSucceed) {
                Log.i("Response", " Success. Detected " + (result == null ? 0 : result.length)
                        + " face(s) in bitmapPicture");
            }

            // Show the result on screen when detection is done.
            setUiAfterDetection(result, mSucceed);
        }

    }



    private String setUiAfterDetection(Face[] result, boolean succeed) {
        // Detection is done, hide the progress dialog.
        mProgressDialog.dismiss();

        if (succeed) {
            // The information about the detection result.
            String detectionResult;
            if (result != null) {
                detectionResult = result.length + " face"
                        + (result.length != 1 ? "s" : "") + " detected";

                ImageView imageView = findViewById(R.id.image);
                imageView.setImageBitmap(rotatedBitmap);

                // Set the adapter of the ListView which contains the details of the detected faces.
                FaceListAdapter faceListAdapter = new FaceListAdapter(result);

                // Show the detailed list of detected faces.
                ListView listView = findViewById(R.id.list_detected_faces);
                listView.setAdapter(faceListAdapter);
            } else {
                detectionResult = "0 face detected";
            }
            return detectionResult;
        }

        bitmapPicture = null;
        return null;
    }


    // The adapter of the GridView which contains the details of the detected faces.
    private class FaceListAdapter extends BaseAdapter {
        // The detected faces.
        List<Face> faces;

        // The thumbnails of detected faces.
        List<Bitmap> faceThumbnails;

        // Initialize with detection result.
        FaceListAdapter(Face[] detectionResult) {
            faces = new ArrayList<>();
            faceThumbnails = new ArrayList<>();

            if (detectionResult != null) {
                // mEditText.setText("detection result not null :)");
                faces = Arrays.asList(detectionResult);
                for (Face face : faces) {
                    try {
                        faceThumbnails.add(ImageHelper.generateFaceThumbnail(
                                rotatedBitmap, face.faceRectangle));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return faces.size();
        }

        @Override
        public Object getItem(int position) {
            return faces.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // Show the face details.
            if (convertView == null) {
                LayoutInflater layoutInflater =
                        (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.item_face_with_description, parent, false);
            }
            convertView.setId(position);

            // Show the face thumbnail.
            ((ImageView) convertView.findViewById(R.id.face_thumbnail)).setImageBitmap(
                    faceThumbnails.get(position));
            final String face_description = String.format("Person # %s is %s, %s looks like %s years old.",
                    faces.size(),
                    getEmotion(faces.get(position).faceAttributes.emotion),
                    getGender((faces.get(position).faceAttributes.gender)),
                    getAge((faces.get(position).faceAttributes.age))

            );

            ((TextView) convertView.findViewById(R.id.text_detected_face)).setText(face_description);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    speakOut(face_description);  //speak after 1000ms
                }
            }, 500);
            //Loop the output :(
            output = face_description;

            return convertView;
        }

        private String getAge(Double age) {
            return String.format("%s", age.intValue());
        }

        private String getGender(String gender){
            String genderType = "";
            if(gender.equals("female")){
                genderType ="she";
            }
            if (gender.equals("male")) {
                genderType="he";
            }
            return String.format("%s", genderType);
        }

        private String getEmotion(Emotion emotion) {
            String emotionType = "";
            double emotionValue = 0.0;
            if (emotion.anger > emotionValue) {
                emotionValue = emotion.anger;
                emotionType = "angry";
            }
            if (emotion.contempt > emotionValue) {
                emotionValue = emotion.contempt;
                emotionType = "contempt";
            }
            if (emotion.disgust > emotionValue) {
                emotionValue = emotion.disgust;
                emotionType = "disgusted";
            }
            if (emotion.fear > emotionValue) {
                emotionValue = emotion.fear;
                emotionType = "fearful";
            }
            if (emotion.happiness > emotionValue) {
                emotionValue = emotion.happiness;
                emotionType = "happy";
            }
            if (emotion.neutral > emotionValue) {
                emotionValue = emotion.neutral;
                emotionType = "neutral";
            }
            if (emotion.sadness > emotionValue) {
                emotionValue = emotion.sadness;
                emotionType = "sad";
            }
            if (emotion.surprise > emotionValue) {
                emotionValue = emotion.surprise;
                emotionType = "surprised";
            }
            return String.format("%s", emotionType);
        }
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.RESULTCAMERA_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDoubleTap() {
        super.onDoubleTap();
        speakOut(output);
    }

    @Override
    public void onLongPress() {
        super.onLongPress();
        startActivity(new Intent(this, Camera_AnalyzeImage.class));
    }

    @Override
    public void onSwipeRight() {
        super.onSwipeRight();
        startActivity(new Intent(this, ExploreActivity.class));
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void speakOut(String text) {
        if(!readyTTS) {
            return;
        } else {
            toSpeech.speak(text, TextToSpeech.QUEUE_ADD, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }

}



