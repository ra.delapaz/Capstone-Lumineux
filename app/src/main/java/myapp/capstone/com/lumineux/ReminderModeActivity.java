package myapp.capstone.com.lumineux;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

public class ReminderModeActivity extends SwipeActivity  implements  TimePickerDialog.OnTimeSetListener, View.OnClickListener{
    TextToSpeech toSpeech;
    public boolean readyTTS;
    private MediaRecorder mediaRecorder;
    private int RECORD_AUDIO_REQUEST_CODE = 123;
    private String fileName = null;
    public boolean isRecording = false;
    public boolean showAlarm;
    private String newFolder = "/Lumineux/VoiceRecords";
    private File audioFile = null;
    private int lastProgress = 0;
    private PendingIntent pendingIntent;
    private Intent myIntent;
    private Calendar dateNow = Calendar.getInstance();
    private Calendar calendar =(Calendar) dateNow.clone();
    SimpleDateFormat simpleDateFormat;
    String Date;
    int selectDay,selectMonth,selectYear,selectHour,selectMinute,yearFinal,monthFinal,dayFinal,dayWeek;
    private ImageButton recordStartButton,recordStopButton;
    public AlarmManager alarmManager;
    public static int request_code = 0;
    private Chronometer chronometer;
    public RelativeLayout relativeLayoutRecorder;
//    public Array Days[] = ["Sunday","Monday","Tuesday"];
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_reminder_mode);

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");

                        Intent installIntent = new Intent();
                        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                        startActivity(installIntent);
                    } else {
                        readyTTS = true;
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        } );

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                speakOut(LumineuxConstants.SPEECH_REMINDER);  //speak after 1000ms
            }
        }, 500); // eto tae

        relativeLayoutRecorder = findViewById(R.id.relativeLayoutRecording);
        chronometer = findViewById(R.id.chronometerTimer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        recordStartButton = findViewById(R.id.recode_audio);
        recordStopButton = findViewById(R.id.record_stop);
        recordStartButton.setOnClickListener(this);
        recordStopButton.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermissionToRecordAudio();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == recordStartButton) {
            prepareRecording();
            startRecording();
            toSpeech.stop();
        } else if(view == recordStopButton) {
            prepareStopRecording();
            stopRecording();
            speakOut("Record Stop");
        }
    }

    public void prepareRecording() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(relativeLayoutRecorder);
        }
        recordStartButton.setVisibility(View.GONE);
        recordStopButton.setVisibility(View.VISIBLE);


    }

    public void startRecording() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        File rootPath = getFilesDir();
        File file = new File(rootPath.getAbsolutePath()+ newFolder);

        if(!file.exists()) {
            file.mkdirs();
        }

        fileName = rootPath.getAbsolutePath() + newFolder + File.separator + String.valueOf("AUDIO-"+System.currentTimeMillis()+ ".mp3");
        Log.d("FILE-NAME",fileName);
        mediaRecorder.setOutputFile(fileName);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        lastProgress = 0;
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    public void prepareStopRecording() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(relativeLayoutRecorder);
        }

        recordStartButton.setVisibility(View.VISIBLE);
        recordStopButton.setVisibility(View.GONE);
    }

    public void stopRecording() {
        try {
            mediaRecorder.stop();
            mediaRecorder.release();
        }catch (Exception e) {
            e.printStackTrace();
        }

        mediaRecorder = null;
        //starting the chronometer
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
//        setDayDialog();
    }

    private void selectAlarm() {
//        DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK,ReminderModeActivity.this,selectYear,selectMonth,selectDay);
//        datePickerDialog.show();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,ReminderModeActivity.this,selectHour,selectMinute,false);
        timePickerDialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getPermissionToRecordAudio() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    RECORD_AUDIO_REQUEST_CODE);
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(this, "Record Audio permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "You must give permissions to use this app. App is exiting.", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        }
    }

//    @Override
//    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//        yearFinal = year;
//        monthFinal = month +1;
//        dayFinal = dayOfMonth;
//
////        calendar.get(Calendar.DAY_OF_WEEK, dayWeek);
//        selectHour = calendar.get(Calendar.HOUR_OF_DAY);
//        selectMinute = calendar.get(Calendar.MINUTE);
//
//
//    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        request_code++;
//        selectHour= hourOfDay;
//        selectMinute = minute ;

//        calendar.set(Calendar.MONTH,monthFinal);
//        calendar.set(Calendar.DAY_OF_MONTH,dayFinal);
//        calendar.set(Calendar.YEAR,yearFinal);
        calendar.set(Calendar.DAY_OF_WEEK,dayWeek);
        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
        calendar.set(Calendar.MINUTE,minute);

        if(calendar.compareTo(dateNow) <= 0) {
            calendar.add(Calendar.DATE, 1);
            Toast.makeText(this, "Today Set time passed, count to tomorrow", Toast.LENGTH_SHORT).show();
        }

//        alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
//        ArrayList<PendingIntent> intentArray = new ArrayList<>();
////        for()
//        myIntent = new Intent(this,Receiver.class);
//
//        myIntent.putExtra("extra","alarm on");
//        pendingIntent = PendingIntent.getBroadcast(this,request_code,myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
//
//        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
    }

    public void stopAlarm(View view) {
        //cancel the alarm
        alarmManager.cancel(pendingIntent);

        //send extra to ringtoneservice
        myIntent.putExtra("extra","alarm off");

        //send broadcast
        sendBroadcast(myIntent);

    }

    public void setDayDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReminderModeActivity.this);
        builder.setTitle("Select a day");
        builder.setSingleChoiceItems(R.array.week, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch(item) {
                    case 0:
                        //Monday
                        dayWeek = 1;
                        selectAlarm();
                        break;
                    case 1:
                        //Tuesday
                       dayWeek = 2;
                        selectAlarm();
                        break;
                    case 2:
                        //Wednesday
                        dayWeek = 3;
                        selectAlarm();
                        break;
                    case 3:
                        //Thursday
                        dayWeek = 4;
                        selectAlarm();
                        break;
                    case 4:
                        //Friday
                        dayWeek =5;
                        selectAlarm();
                        break;
                    case 5:
                        //Saturday
                        dayWeek =6;
                        selectAlarm();
                }
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.REMINDER_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onSwipeRight() {
        super.onSwipeRight();
        startActivity(new Intent(ReminderModeActivity.this, RecordingItemActivity.class));
        Log.d("Debug","onSwipeRight");
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void speakOut(String text){
        if(!readyTTS){
           // Toast.makeText(this,"Text to Speech not ready ", Toast.LENGTH_LONG).show();
            return;
        } else {
           // Toast.makeText(this,"Text To Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
