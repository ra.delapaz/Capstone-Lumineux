package myapp.capstone.com.lumineux;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;

import myapp.capstone.com.lumineux.camera.Camera_AnalyzeImage;
import myapp.capstone.com.lumineux.camera.Camera_BankNotes;
import myapp.capstone.com.lumineux.camera.Camera_RecognizeText;
import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;


public class ExploreActivity extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    Button btnImage, btnText, btnBank;

    public boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if(netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public AlertDialog.Builder buildDialog(Context c) {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setCancelable(false);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press Ok to return to the Main Menu");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ExploreActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        return builder;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View settingView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        settingView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.explore_buttons);

        if(!isConnected(ExploreActivity.this)) buildDialog(ExploreActivity.this).show();
        else {
            settingView.setSystemUiVisibility(uiOptions);
            setContentView(R.layout.explore_buttons);

            toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if(status == TextToSpeech.SUCCESS){
                        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                        float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                        String speechVoice = sharedPreferences.getString("speechVoice", "");
                        Locale voice = null;

                        if (speechVoice.equals("maleVoice")) {
                            voice = Locale.UK;
                        } else if (speechVoice.equals("femaleVoice")) {
                            voice = Locale.US;
                        } else {
                            voice = Locale.getDefault();
                        }

                        int result = toSpeech.setLanguage(voice);
                        toSpeech.setSpeechRate(speechRate);

                        if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                            Log.e("TTS", "This Language is not supported");
                        } else {
                            readyTTS = true;
                        }
                    } else {
                        Log.e("TTS", "Initilization Failed!");
                    }
                }
            } );

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    speakOut(LumineuxConstants.SPEECH_EXPLORE);  //speak after 1000ms
                }
            }, 500); // eto tae
        }

        btnBank = findViewById(R.id.btn_bank);
        btnText = findViewById(R.id.btn_text);
        btnImage = findViewById(R.id.btn_image);

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExploreActivity.this, Camera_AnalyzeImage.class);
                startActivity(intent);
               // Toast.makeText(view.getContext(),"Analyze Image Activity", Toast.LENGTH_LONG).show();
            }
        });

        btnImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                speakOut(LumineuxConstants.ANALYZEIMAGE_INTRO);
                return true;
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExploreActivity.this, Camera_RecognizeText.class);
                startActivity(intent);
                //Toast.makeText(view.getContext(),"Recognize Text Activity", Toast.LENGTH_LONG).show();
            }
        });

        btnText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                speakOut(LumineuxConstants.RECOGNIZETEXT_INTRO);
                return true;
            }
        });

        btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExploreActivity.this, Camera_BankNotes.class);
                startActivity(intent);
               // Toast.makeText(view.getContext(),"Bank Notes Activity", Toast.LENGTH_LONG).show();
            }
        });

        btnBank.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                speakOut(LumineuxConstants.BILLS_INTRO);
                return true;
            }
        });
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.EXPLORE_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onSwipeDown() {
        super.onSwipeDown();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void speakOut(String text) {
        if(!readyTTS) {
          //  Toast.makeText(this,"Text To Speech not ready ", Toast.LENGTH_LONG).show();
            return;
        } else {
           // Toast.makeText(this,"Text To Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
