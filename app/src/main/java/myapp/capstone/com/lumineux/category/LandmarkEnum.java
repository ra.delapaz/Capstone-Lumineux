package myapp.capstone.com.lumineux.category;

import com.microsoft.projectoxford.vision.contract.Tag;

/**
 * Created by user on 4/2/2018.
 */

public enum LandmarkEnum {
    sculpture,
    building,
    outdoor,
    sky,
    stone,
    church,
    old,
    statue,
    mountain,
    grass,
    field,
    tower,
    hill;

    public static boolean hasLandmark(String tag){
        for(LandmarkEnum l : values()){
           if (l.name().equals(tag))
               return true;
        }
        return false;
    }

}
