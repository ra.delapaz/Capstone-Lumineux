package myapp.capstone.com.lumineux;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

public class RecordingItemActivity extends SwipeActivity {
    TextToSpeech toSpeech;
    public boolean readyTTS;
    private RecyclerView recyclerViewRecordings;
    private ArrayList<Recording> recordingArrayList;
    private RecordingListAdapter recordingListAdapter;
    private TextView textViewNoRecordings;
    private String newFolder = "/Lumineux/VoiceRecords";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View main = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        main.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.activity_recording_item);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");

                        Intent installIntent = new Intent();
                        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                        startActivity(installIntent);
                    } else {
                        readyTTS = true;
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        } );

        recordingArrayList = new ArrayList<Recording>();
        recyclerViewRecordings = findViewById(R.id.recyclerViewRecordings);
        recyclerViewRecordings.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        recyclerViewRecordings.setHasFixedSize(true);
        textViewNoRecordings = findViewById(R.id.textViewNoRecordings);

        getRecordings();
    }

    //Lists the audio files
    public void getRecordings() {
        File rootPath  = getFilesDir();
        String path = rootPath.getAbsolutePath() + newFolder;
        Log.d("FILE-CONTENTS", "Path: " +path);
        File audioDirectory = new File(path);
        File[] files = audioDirectory.listFiles();
//        Log.d("LIST-FILES","Size: "+files.length);
        if(files !=null) {
            for (int i = 0; i < files.length; i++){
                Log.d("FILE-CONTENTS","FileName:"+ files[i].getName());
                String fileName = files[i].getName();
                String recordingUri = rootPath.getAbsolutePath() + newFolder + File.separator + fileName;

                Recording recording = new Recording(recordingUri,fileName,false);
                recordingArrayList.add(recording);
            }

            textViewNoRecordings.setVisibility(View.GONE);
            recyclerViewRecordings.setVisibility(View.VISIBLE);
            setAdapterRecyclerView();

        } else {
            textViewNoRecordings.setVisibility(View.VISIBLE);
            recyclerViewRecordings.setVisibility(View.GONE);
        }
    }

    public void getRingtone() {
        File rootPath  = getFilesDir();
        String path = rootPath.getAbsolutePath() + newFolder;
        Log.d("FILE-CONTENTS", "Path: " +path);
        File audioDirectory = new File(path);
        File[] files = audioDirectory.listFiles();
//        Log.d("LIST-FILES","Size: "+files.length);
        if(files !=null) {
            for (int i = 0; i < files.length; i++) {
                Log.d("FILE-CONTENTS","FileName:"+ files[i].getName());
                String fileName = files[i].getName();
                String recordingUri = rootPath.getAbsolutePath() + newFolder + File.separator + fileName;
                Recording recording = new Recording(recordingUri,fileName,false);
                recordingArrayList.add(recording);
            }
        } else {
            Toast.makeText(this, "No Audios!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setAdapterRecyclerView() {
        recordingListAdapter = new RecordingListAdapter(getApplicationContext(),recordingArrayList);
        recyclerViewRecordings.setAdapter(recordingListAdapter);
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.REMINDERLIST_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onSwipeLeft() {
        startActivity(new Intent(this, ReminderModeActivity.class));
    }

    public void speakOut(String text){
        if(!readyTTS){
           // Toast.makeText(this,"Text to Speech not ready ", Toast.LENGTH_LONG).show();
            return;
        } else {
           // Toast.makeText(this,"Text To Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
