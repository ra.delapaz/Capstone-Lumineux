package myapp.capstone.com.lumineux;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import java.io.FileInputStream;

public class Receiver extends AppCompatActivity{


    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String filename = getIntent().getStringExtra("image");
        Bitmap bitmapPicture = null;
        try{
           FileInputStream is = this.openFileInput(filename);
           bitmapPicture = BitmapFactory.decodeStream(is);
           is.close();
       }catch (Exception e){
           e.printStackTrace();
       }

        setContentView(R.layout.receive_bitmap);
        ImageView viewBtimap = findViewById(R.id.bitmapview);
        viewBtimap.setImageBitmap(bitmapPicture);
        viewBtimap.setRotation(90);


    }

}


