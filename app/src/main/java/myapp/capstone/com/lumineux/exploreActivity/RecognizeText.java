package myapp.capstone.com.lumineux.exploreActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.HandwritingRecognitionOperation;
import com.microsoft.projectoxford.vision.contract.HandwritingRecognitionOperationResult;
import com.microsoft.projectoxford.vision.contract.HandwritingTextLine;
import com.microsoft.projectoxford.vision.contract.HandwritingTextWord;
import com.microsoft.projectoxford.vision.rest.VisionServiceException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Locale;

import myapp.capstone.com.lumineux.ExploreActivity;
import myapp.capstone.com.lumineux.MainActivity;
import myapp.capstone.com.lumineux.R;
import myapp.capstone.com.lumineux.camera.Camera_RecognizeText;
import myapp.capstone.com.lumineux.constants.LumineuxConstants;
import myapp.capstone.com.lumineux.utility.SwipeActivity;

public class RecognizeText extends SwipeActivity {
    //COMMENT 7:56 pm! 4/08/2018
    private TextView mEditText;
    private static final String TAG = "Recognize Text" ;
    //max retry times to get operation result
    private int retryCountThreshold = 30;
    private VisionServiceClient client;
    private Bitmap bitmapPicture;
    private Uri filename;
    public String output;
    ProgressDialog mProgressDialog;
    private RelativeLayout bitmaprelative;
    TextToSpeech toSpeech;
    public boolean readyTTS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analyzepicturelayout);

        // Get a Uri from an Intent

        filename = getIntent().getParcelableExtra("imageUri");

        bitmaprelative = findViewById(R.id.analyze_layout);

        try {
            bitmapPicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filename);
        } catch (Exception e) {
            e.printStackTrace();
        }

        toSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    float speechRate = sharedPreferences.getFloat("speechRate", 1.0f);
                    String speechVoice = sharedPreferences.getString("speechVoice", "");
                    Locale voice = null;

                    if (speechVoice.equals("maleVoice")) {
                        voice = Locale.UK;
                    } else if (speechVoice.equals("femaleVoice")) {
                        voice = Locale.US;
                    } else {
                        voice = Locale.getDefault();
                    }

                    int result = toSpeech.setLanguage(voice);
                    toSpeech.setSpeechRate(speechRate);

                    if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        readyTTS = true;
                        Log.e("TTS", "readyTTS True");
                    }
                } else {
                    Log.e("TTS", "Initialization Failed!");
                }
            }
        } );

        ImageView viewBitmap = findViewById(R.id.bitmapview);
        mEditText = findViewById(R.id.editTextResult);
        mEditText.setMovementMethod(new ScrollingMovementMethod());
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setTitle(getString(R.string.progress_dialog_title));

        // find the width and height of the screen:
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        // scale it to fit the screen, x and y swapped because my image is wider than it is tall
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

        // create a matrix object
        Matrix matrix = new Matrix();
        matrix.postRotate(90); // anti-clockwise by 90 degrees

        // create a new bitmap from the original using the matrix to transform the result
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

        BitmapDrawable data2 = new BitmapDrawable(getResources(), rotatedBitmap);
        Log.i(TAG, "rotated bitmap");

        bitmaprelative.setBackground(data2);

        if (client==null){
            client = new VisionServiceRestClient(getString(R.string.subscription_key), getString(R.string.subscription_apiroot));
        }

        mEditText = findViewById(R.id.editTextResult);
        doRecognize();

    }
    public void doRecognize() {
        try {
            new doRequest(this).execute();
        } catch (Exception e) {
            mEditText.setText("Error encountered. Exception is: " + e.toString());
        }
    }
    private String process() throws VisionServiceException, IOException, InterruptedException {
        Gson gson = new Gson();
        // Put the image into an input stream for detection.
        Display d = getWindowManager().getDefaultDisplay();
        int x = d.getWidth();
        int y = d.getHeight();

        try (ByteArrayOutputStream output = new ByteArrayOutputStream()){
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, y, x, true);

            // create a matrix object
            Matrix matrix = new Matrix();
            matrix.postRotate(90); // anti-clockwise by 90 degrees

            // create a new bitmap from the original using the matrix to transform the result
            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);

            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray())){
                //post image and got operation from API
                HandwritingRecognitionOperation operation = this.client.createHandwritingRecognitionOperationAsync(inputStream);

                HandwritingRecognitionOperationResult operationResult;
                //try to get recognition result until it finished.

                int retryCount = 0;
                do {
                    if (retryCount > retryCountThreshold) {
                        throw new InterruptedException("Can't get result after retry in time.");
                    }
                    Thread.sleep(1000);
                    operationResult = this.client.getHandwritingRecognitionOperationResultAsync(operation.Url());
                }
                while (operationResult.getStatus().equals("NotStarted") || operationResult.getStatus().equals("Running"));

                String result = gson.toJson(operationResult);
                Log.d("result", result);
                return result;
            }catch (Exception ex) {
                throw ex;
            }
        }catch (Exception ex){
            throw ex;
        }
    }

    private class doRequest extends AsyncTask<String, String, String> {
        private Exception e = null;
        private WeakReference<RecognizeText> recognitionActivity;

        public doRequest(RecognizeText activity) {
            recognitionActivity = new WeakReference<RecognizeText>(activity);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
            Log.i("Request", "Analyzing image...");
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                if (recognitionActivity.get() != null) {
                    return recognitionActivity.get().process();
                }
            } catch (Exception e) {
                this.e = e;    // Store error
            }
            return null;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            mProgressDialog.dismiss();

            if (recognitionActivity.get() == null) {
                return;
            }
            // Display based on error existence
            if (e != null) {
                recognitionActivity.get().mEditText.setText("Error: " + e.getMessage());
                final String errorMessage = e.getMessage();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        speakOut(errorMessage);  //speak after 1000ms
                    }
                }, 500);

                mEditText.setText(errorMessage);
                output = errorMessage;
                this.e = null;
            } else {
                Gson gson = new Gson();
                HandwritingRecognitionOperationResult r = gson.fromJson(data, HandwritingRecognitionOperationResult.class);
                StringBuilder resultBuilder = new StringBuilder();
                if (r.getStatus().equals("Failed")) {

                    resultBuilder.append("Error: Recognition Failed");
                } else {
                    for (HandwritingTextLine line : r.getRecognitionResult().getLines()) {
                        for (HandwritingTextWord word : line.getWords()) {
                            resultBuilder.append(word.getText() + " ");
                        }
                        resultBuilder.append("");
                    }
                    resultBuilder.append("\n");
                }

                output = resultBuilder.toString();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        speakOut(output);  //speak after 1000ms
                    }
                }, 500);


                recognitionActivity.get().mEditText.setText(resultBuilder);
            }
        }
    }

    public void tutorial (View view) {
        speakOut(LumineuxConstants.RESULTCAMERA_TUTORIAL);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        speakOut("Back button is disabled within the application");
        builder.setMessage("Back button is disabled within the application")
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDoubleTap() {
        super.onDoubleTap();
        speakOut(output);
    }

    @Override
    public void onLongPress() {
        super.onLongPress();
        startActivity(new Intent(this, Camera_RecognizeText.class));
    }

    @Override
    public void onSwipeRight() {
        super.onSwipeRight();
        startActivity(new Intent(this, ExploreActivity.class));
    }

//    @Override
//    public void onSwipeDown() {
//        super.onSwipeDown();
//        startActivity(new Intent(this, MainActivity.class));
//    }

    public void speakOut(String text) {
        if(!readyTTS) {
           // Toast.makeText(this,"Text to Speech not ready ", Toast.LENGTH_LONG).show();
            return;
        } else {
           // Toast.makeText(this,"Text To Speech ready", Toast.LENGTH_LONG).show();
            toSpeech.speak(text, LumineuxConstants.queueType, null);
        }
    }

    @Override
    protected void onPause() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (toSpeech != null) {
            toSpeech.stop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown!
        if (toSpeech != null) {
            toSpeech.stop();
            toSpeech.shutdown();
        }
        super.onDestroy();
    }
}
